package com.instaoffice.app.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.instaoffice.app.R;
import com.instaoffice.app.adapters.ExpandableListAdapter;
import com.instaoffice.app.fragments.Navigation_frag;
import com.instaoffice.app.models.DetailsOfUser;
import com.instaoffice.app.utils.CommonUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BookMeetingRoom extends AppCompatActivity implements Navigation_frag.NavigationDrawerCallbacks, AdapterView.OnItemSelectedListener {

    //for expandable view
    ExpandableListAdapter listAdapter,timeExpandListViewAddapter;
    static ExpandableListView expListView;
    static List<String> listDataHeader,headingTimeHeader;
    HashMap<String, List<String>> listDataChild,listTimeChild;
    static TextView cityTextName,selectcityText,selectCenter,ceneterName;
    static TextView yourReqirementText,meetingroomTypeText,durationText,errormsgOfValidaationProceedButton;
    TextView selectedMeetingRoomType,selectedDuration, confirmAndPay,errorMsgofOtp,mobile_header_text;
    static ImageView arrowImgselectcity,arrowImgselectCenter, arrowImageRequiremnt,arrowImageDuration, arrowImageRequirementMain;
    static String activeLinear="none";

    LinearLayout generateOtpCodeLayout,submitOtpLayout;
    static LinearLayout requirmentSubLinear, linearLayoutToHide,linearLayoutSelectCity,linearLayoutSelectCenter,linearyourRequirment,linearSubMeetingType;
    static LinearLayout linearCheckandPay,linearSubCheckandPay,hSLinearLayout,linearButtonOfTime,linearCheckboxes;
    Button proceedButton,payNowButton,changePhoneNumberButton,generateOtpLayoutButton, submitOTpButton;
    TextView dateButton;
    RelativeLayout dateView;
    EditText fname_editbox,email_editbox,mobile_editbox, otpCodeEditBox;

    RelativeLayout changeNumberButtonLayout,additionalRequirement;
    static TextView totalCostText, yourReqmtDetails;
    public static Spinner meetingSpinner;

    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
    static final int DATE_DIALOG_ID = 999;
    //time button
    private int pHour;
    private int pMinute;
    String AM_PM ;
    /** This integer will uniquely define the dialog to be used for displaying time picker.*/
    static final int TIME_DIALOG_ID = 0;

    int extraCost=0;

    TextView errormsgName,errormsgEmail,errormsgMobile, mobile_text;
    int siz;
    Typeface typeface;

    public String isActivatePayButton="false";

    //Validations For Payment
    String citySelected="null",centerSelected="null",MeetinRoomTypeSelected="null";
    String dateSelected="null";
    public static  String timeSelected="null";
    String nameEntered="null",emailEntered="null", mobileEntered="null";
    String previousCitySelected;
    public static final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for setting fullScreen Method
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.book_meeting_room_activity);

       // Toast.makeText(this, "="+System.currentTimeMillis(), Toast.LENGTH_SHORT).show();
        //setting the date font
        typeface = Typeface.createFromAsset(getAssets(), "fonts/WorkSans-Medium.otf");

        CommonUtil cu=new CommonUtil(this);
        cu.getCitiesListthroughApi();
        //  cu.getAllCouponCodes();

        dateView=(RelativeLayout) findViewById(R.id.linearDate);

        errormsgOfValidaationProceedButton=(TextView) findViewById(R.id.errorMsgOfProcced) ;

        additionalRequirement=(RelativeLayout) findViewById(R.id.additionalRequirements);
        additionalRequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(linearCheckboxes.getVisibility()==View.VISIBLE){
                    linearCheckboxes.setVisibility(View.GONE);
                }else{
                    linearCheckboxes.setVisibility(View.VISIBLE);
                }

            }
        });

        //Button
        submitOtpLayout=(LinearLayout) findViewById(R.id.submitOtp)  ;

        generateOtpCodeLayout=(LinearLayout)findViewById(R.id.generateOtpCode);
        changeNumberButtonLayout=(RelativeLayout)findViewById(R.id.changeNumberLayout) ;
        changePhoneNumberButton=(Button) findViewById(R.id.changePhoneNumber);
        changePhoneNumberButton.setTypeface(typeface);
        changePhoneNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isActivatePayButton="false";
                changeNumberButtonLayout.setVisibility(View.GONE);
                generateOtpCodeLayout.setVisibility(View.VISIBLE);
                if(submitOtpLayout.getVisibility()==View.VISIBLE){
                    submitOtpLayout.setVisibility(View.GONE);
                }
                mobileEntered="null";
                showNameAndEmailEditBox(false);
            }
        });
        mobile_header_text=(TextView)findViewById(R.id.ymb_text);
        mobile_text=(TextView) findViewById(R.id.mobile_text);
        generateOtpLayoutButton=(Button) findViewById(R.id.generateOtpButton);
        generateOtpLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mobile_editbox.getText().toString().length()==10){
                    //call api
                    mobile_header_text.setText("We have sent an OTP to");
                    mobile_text.setText("+91 "+mobile_editbox.getText().toString());
                    changeNumberButtonLayout.setVisibility(View.VISIBLE);
                    generateOtpCodeLayout.setVisibility(View.GONE);
                    submitOtpLayout.setVisibility(View.VISIBLE);
                    mobileEntered="true";
                    DetailsOfUser.MobileNumber=Long.parseLong(mobile_editbox.getText().toString());

                    sendOtp();

                }else{
                    errormsgMobile.setVisibility(View.VISIBLE);
                    mobile_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_red);
                    mobile_editbox.setHintTextColor(Color.parseColor("#e70000"));
                    mobileEntered="null";
                }
            }
        });

        errorMsgofOtp=(TextView)findViewById(R.id.errorMsgofOtp);
        submitOTpButton =(Button)findViewById(R.id.verify_button);

        otpCodeEditBox=(EditText) findViewById(R.id.otpCode_editbox);
        otpCodeEditBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpCodeEditBox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                errorMsgofOtp.setVisibility(View.GONE);
            }
        });

        submitOTpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(otpCodeEditBox.getText().toString().equalsIgnoreCase("") || otpCodeEditBox.getText().toString().equalsIgnoreCase(" ")){
                    errorMsgofOtp.setVisibility(View.VISIBLE);
                }else{
                    verifyOtp();
                }
            }
        });


        dateButton =(TextView) findViewById(R.id.dateButton);
     //   dateButton.setTypeface(typeface);
      //  dateButton.setTransformationMethod(null);

        meetingSpinner=(Spinner) findViewById(R.id.spinner);
        meetingSpinner.setOnItemSelectedListener(this);

        payNowButton=(Button) findViewById(R.id.payNowButton);
        payNowButton.setTypeface(typeface);
        proceedButton=(Button) findViewById(R.id.proceedbtn) ;
        proceedButton.setTypeface(typeface);

        errormsgName=(TextView)findViewById(R.id.errorTextname) ;
        errormsgEmail=(TextView)findViewById(R.id.errorTextEmail) ;
        errormsgMobile=(TextView)findViewById(R.id.error_msg_editbox_mobile) ;

        fname_editbox=(EditText) findViewById(R.id.editbox_name);
        email_editbox=(EditText) findViewById(R.id.editbox_email);
        mobile_editbox=(EditText) findViewById(R.id.editbox_mobile);

        fname_editbox.setTypeface(typeface);
        email_editbox.setTypeface(typeface);
        mobile_editbox.setTypeface(typeface);
        otpCodeEditBox.setTypeface(typeface);

        cityTextName=(TextView)findViewById(R.id.cityNameText);
        selectcityText=(TextView) findViewById(R.id.selectcityText);

        ceneterName = (TextView) findViewById(R.id.centerName);
        selectCenter = (TextView) findViewById(R.id.selectCenter);

        yourReqmtDetails=(TextView)findViewById(R.id.subYourRequirement);

        totalCostText=(TextView) findViewById(R.id.rstext);
      //  totalCostText.setText("RS "+String.valueOf(totalAmountOfRoom+extraCost));

        yourReqirementText=(TextView)findViewById(R.id.yourRequirment);
        confirmAndPay=(TextView) findViewById(R.id.confirmAndPay);
        linearLayoutToHide=(LinearLayout) findViewById(R.id.lineartohide);
        arrowImgselectCenter = (ImageView) findViewById(R.id.imagearrow2);
        arrowImageRequirementMain=(ImageView) findViewById(R.id.imagearrow3);
        arrowImgselectcity=(ImageView) findViewById(R.id.imagearrow);

        meetingroomTypeText=(TextView) findViewById(R.id.meetingRTypetext);
        //selectedMeetingRoomType =(TextView) findViewById(R.id.selectedRoomType);
        arrowImageRequiremnt=(ImageView) findViewById(R.id.imagearrow5);

        linearLayoutSelectCenter=(LinearLayout) findViewById(R.id.linearSelectCenter);
        linearyourRequirment=(LinearLayout)findViewById(R.id.linearyourRequirment) ;
        requirmentSubLinear=(LinearLayout)findViewById(R.id.requirmentSubLinear) ;
        linearSubMeetingType=(LinearLayout)findViewById(R.id.linearSubMeetingType) ;
        linearCheckandPay=(LinearLayout)findViewById(R.id.linearcheckAndPay);
        linearSubCheckandPay=(LinearLayout)findViewById(R.id.linearSubCandP);
        linearCheckboxes=(LinearLayout)findViewById(R.id.linearcheckboxes);
        linearButtonOfTime=(LinearLayout)findViewById(R.id.toDisplayButton);


//for horizontal scroll images
        int[] images = new int[]{R.drawable.horizontalimage1,R.drawable.horizontalimage1,R.drawable.horizontalimage1, R.drawable.horizontalimage1};
        hSLinearLayout = (LinearLayout) findViewById (R.id.HsLinear);
        for (int i=0 ; i<4; i++){
            ImageView iv = new ImageView (this);
            iv.setImageResource (images[i]);
            iv.setPadding(10,10,10,10);
            final int finalI = i;
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // Toast.makeText(BookMeetingRoom.this, "Clicked On "+ finalI, Toast.LENGTH_SHORT).show();
                }
            });
            hSLinearLayout.addView(iv);
        }


        fname_editbox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                fname_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                errormsgName.setVisibility(View.GONE);
                return false;
            }
        });
        email_editbox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                email_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                errormsgEmail.setVisibility(View.GONE);
                return false;
            }
        });
        mobile_editbox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mobile_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                mobile_editbox.setHintTextColor(Color.parseColor("#e11d1d1d"));
                errormsgMobile.setVisibility(View.GONE);
                return false;
            }
        });

        arrowImageRequirementMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(linearLayoutToHide.getVisibility()==View.VISIBLE){
                    linearLayoutToHide.setVisibility(View.GONE);
                    yourReqirementText.setText("Requirements");
                    yourReqirementText.setPadding(30, 6, 0, 12);
                    yourReqirementText.setTextColor(Color.parseColor("#D5D5D5"));
                    linearSubCheckandPay.setVisibility(View.GONE);
                    confirmAndPay.setTextColor(Color.parseColor("#C8C7C6"));

                }else{
                    linearLayoutToHide.setVisibility(View.VISIBLE);
                    yourReqmtDetails.setVisibility(View.GONE);
                    yourReqirementText.setText("YOUR REQUIREMENTS");
                    yourReqirementText.setPadding(30, 6, 0, 12);
                    yourReqirementText.setTextColor(Color.parseColor("#f55416"));
                }
                if(linearSubCheckandPay.getVisibility()==View.VISIBLE){
                    linearSubCheckandPay.setVisibility(View.GONE);
                    confirmAndPay.setTextColor(Color.parseColor("#C8C7C6"));
                    // confirmAndPay.setTextSize(18);
                }

            }
        });

        //Date
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);


        linearLayoutSelectCity=(LinearLayout) findViewById(R.id.linearSelectCity);
        linearLayoutSelectCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearSelectCityfunction();

            }
        });


        linearLayoutSelectCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(citySelected.equalsIgnoreCase("null"))) {

                    linear2function();

                }
            }
        });

        linearyourRequirment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!centerSelected.equalsIgnoreCase("null")) {
                    openYourRequiremnt();
                    linearSubCheckandPay.setVisibility(View.GONE);
                    confirmAndPay.setTextColor(Color.parseColor("#C8C7C6"));
                    //  confirmAndPay.setTextSize(18);
                }

            }
        });

        linearSubMeetingType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!activeLinear.equalsIgnoreCase("meetingType")){
                    expListView.collapseGroup(0);
                    linearSubMeetingType.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                }

                meetingroomTypeText.setVisibility(View.GONE);
                //selectedMeetingRoomType.setVisibility(View.GONE);
                arrowImageRequiremnt.setVisibility(View.GONE);
                activeLinear="meetingType";

              //  expListView = (ExpandableListView) findViewById(R.id.Expandable3);
                //expListView.setVisibility(View.VISIBLE);
               // startExpandables();
               // expListView.expandGroup(0);

                meetingSpinner.setVisibility(View.VISIBLE);
                meetingSpinner.performClick();

            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validationOfproceed();
            }
        });

        payNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isActivatePayButton.equalsIgnoreCase("true")) {
                    validationForCheckAndPay();
                }
            }

        });
    }


    public void startExpandables(){

        prepareListData();
        listAdapter = new ExpandableListAdapter(getApplicationContext(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {



            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                //     Toast.makeText(getApplicationContext(),
                //       listDataHeader.get(groupPosition) + " Expanded",
                //      Toast.LENGTH_SHORT).show();
                int height = 0;
                float density = getApplicationContext().getResources().getDisplayMetrics().density;

                if (density >= 4.0) {
                    height=22;
                }else if (density >= 3.0) {
                    height=18;
                }else if (density >= 2.0) {
                    height=0;
                }

                for (int i = 0; i < siz; i++) {
                    height +=9 ;
                    // height += expListView.getDividerHeight();
                }
                expListView.getLayoutParams().height = (height+6)*10;
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                //  Toast.makeText(getApplicationContext(),
                //          listDataHeader.get(groupPosition) + " Collapsed",
                //          Toast.LENGTH_SHORT).show();

                //expListView.expandGroup(0);
                expListView.setVisibility(View.GONE);
                if(activeLinear.equalsIgnoreCase("linearSelectCity")  ) {
                    if (citySelected.equalsIgnoreCase("null")) {
                        selectcityText.setVisibility(View.VISIBLE);
                        arrowImgselectcity.setVisibility(View.VISIBLE);
                        // citySelected="null";
                        selectcityText.setText("SELECT YOUR CITY");
                        // selectcityText.setTextSize(18);
                    } else {
                        selectcityText.setVisibility(View.VISIBLE);
                        cityTextName.setVisibility(View.VISIBLE);
                        arrowImgselectcity.setVisibility(View.VISIBLE);


                    }
                }
                if(activeLinear.equalsIgnoreCase("linearSelectCenter")) {
                    if ( centerSelected.equalsIgnoreCase("null")) {
                        selectCenter.setVisibility(View.VISIBLE);
                        arrowImgselectCenter.setVisibility(View.VISIBLE);
                        // centerSelected="null";
                        selectCenter.setText("SELECT YOUR CENTER");
                        //selectCenter.setTextSize(18);
                    } else {
                        selectCenter.setVisibility(View.VISIBLE);
                        ceneterName.setVisibility(View.VISIBLE);
                        arrowImgselectCenter.setVisibility(View.VISIBLE);
                    }
                }

                if(activeLinear.equalsIgnoreCase("meetingType")){
                    if( MeetinRoomTypeSelected.equalsIgnoreCase("null")){
                        meetingroomTypeText.setVisibility(View.VISIBLE);
                        arrowImageRequiremnt.setVisibility(View.VISIBLE);
                        // meetingroomTypeText.setTextSize(16);
                        // MeetinRoomTypeSelected="null";
                        //selectedMeetingRoomType.setVisibility(View.GONE);
                    }else {
                      //  selectedMeetingRoomType.setVisibility(View.VISIBLE);
                        meetingroomTypeText.setVisibility(View.VISIBLE);
                        arrowImageRequiremnt.setVisibility(View.VISIBLE);
                    }
                }

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub

                expListView.setVisibility(View.GONE);
                if(activeLinear.equalsIgnoreCase("linearSelectCity") ) {
                    selectcityText.setVisibility(View.VISIBLE);
                    arrowImgselectcity.setVisibility(View.VISIBLE);
                    selectcityText.setText("Your city");
                    cityTextName.setTextColor(Color.parseColor("#f55416"));
                    cityTextName.setVisibility(View.VISIBLE);
                    selectcityText.setTextColor(Color.parseColor("#D5D5D5"));
                    //validation text
                    citySelected=listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                    if(!cityTextName.getText().toString().equalsIgnoreCase(citySelected)){
                        // Toast.makeText(BookMeetingRoom.this, citySelected +"  "+previousCitySelected, Toast.LENGTH_SHORT).show();
                        resetValueForAll();
                    }
                    CommonUtil.CheckAndCallApiForSubListOfCities(childPosition);
                    cityTextName.setText(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                    // CommonUtil.getTheMeetingRoomTypeThroughApi();
                    //   linear2function();

                }   else if(activeLinear.equalsIgnoreCase("linearSelectCenter")) {

                    selectCenter.setVisibility(View.VISIBLE);
                    arrowImgselectCenter.setVisibility(View.VISIBLE);
                    // selectCenter.setTextSize(14);
                    selectCenter.setText("Your location");
                    selectCenter.setTextColor(Color.parseColor("#D5D5D5"));
                    ceneterName.setTextColor(Color.parseColor("#f55416"));
                    ceneterName.setVisibility(View.VISIBLE);
                    meetingSpinner.setVisibility(View.GONE);
                    ceneterName.setText(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                    yourReqirementText.setTextColor(Color.parseColor("#636363"));

                    if(!centerSelected.equalsIgnoreCase(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition))) {
                        resetValuesForRequirement();
                    }
                    openYourRequiremnt();
                    centerSelected=listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                    CommonUtil.checkAndCallApiForMeetingType(childPosition);
                }
                //  Toast.makeText(getApplicationContext(),listDataHeader.get(groupPosition)+ " : "+ listDataChild.get(listDataHeader.get(groupPosition)).get(
                //      childPosition), Toast.LENGTH_SHORT)
                //     .show();
                return false;
            }
        });

    }
    /*
      * Preparing the list data
      */
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        if(activeLinear.equalsIgnoreCase("linearSelectCity")) {
            // Adding child data
            listDataHeader.add("SELECT YOUR CITY");
            listDataChild.put(listDataHeader.get(0),CommonUtil.cityNametoDisplay); // Header, Child data
            siz=CommonUtil.cityNametoDisplay.size();
        }
        if(activeLinear.equalsIgnoreCase("linearSelectCenter")){
            listDataHeader.add("SELECT YOUR CENTRE");
            listDataChild.put(listDataHeader.get(0), CommonUtil.centerNametoDisplay);
            siz=CommonUtil.centerNametoDisplay.size();
        }
        if(activeLinear.equalsIgnoreCase("meetingType")){
            listDataHeader.add("Type");
            //    List<String> meetingRoomType = new ArrayList<String>();
            //    meetingRoomType.add("SMALL");
            //   meetingRoomType.add("MEDIUM");
            //   meetingRoomType.add("LARGE");
            // listDataChild.put(listDataHeader.get(0), meetingRoomType);
            listDataChild.put(listDataHeader.get(0), CommonUtil.meetingRoomTypeNametoDisplay);
            // siz=meetingRoomType.size();
            siz=CommonUtil.meetingRoomTypeNametoDisplay.size();
        }

    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        dateView.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,myDateListener, year, month, day);
        }
        if (id == 0) {
            return new TimePickerDialog(this,
                    mTimeSetListener, pHour, pMinute, false);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day

                    showDate(arg1, arg2+1, arg3);
                }
            };
    private void showDate(int year, int month, int day) {
        String textTime= (day+" "+MONTHS[month-1]+" "+year)+"";
        dateButton.setTextColor(Color.parseColor("#f55416"));
        dateButton.setText(textTime);

        dateSelected=day+" "+MONTHS[month-1]+" "+year;

        //to get the schedule of the booking
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        String startTime = (day+"/"+month+"/"+year+" 09:00");
        String endTime = (day+"/"+month+"/"+year+" 18:00");
        Date startMilisecs = null;
        Date endMilisecs = null;
        try {
            startMilisecs = formatter.parse(startTime);
            endMilisecs = formatter.parse(endTime);
            DetailsOfUser.milliSecondsOfStart=startMilisecs.getTime();
            DetailsOfUser.milliSecondsOfEnd=endMilisecs.getTime();
            CommonUtil.resetAllButtonsOnDateCahanged();
            CommonUtil.getScheduleAfterDateSelection(String.valueOf(startMilisecs.getTime()),String.valueOf(endMilisecs.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    if(hourOfDay < 12) {
                        AM_PM = "AM";
                    } else {
                        AM_PM = "PM";
                    }
                    pHour = hourOfDay;
                    pMinute = minute;
                    //updateDisplay();
                }
            };

    public void validationOfproceed(){

        if(citySelected.equalsIgnoreCase("null")){
            linearLayoutSelectCity.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else {
            linearLayoutSelectCity.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(centerSelected.equalsIgnoreCase("null")){
            linearLayoutSelectCenter.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else{
            linearLayoutSelectCenter.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(MeetinRoomTypeSelected.equalsIgnoreCase("null")){
            linearSubMeetingType.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else{
            linearSubMeetingType.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(dateSelected.equalsIgnoreCase("null")){
            dateView.setBackgroundResource(R.drawable.transparent_bg_corner_red);

        }else{
            dateView.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(CommonUtil.fromTime==-1){
            linearButtonOfTime.setBackgroundResource(R.drawable.transparent_bg_corner_red);
            timeSelected="null";
        }else{
            linearButtonOfTime.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
            timeSelected="true";
        }

        //for taking it
        if(citySelected.equalsIgnoreCase("null")|| centerSelected.equalsIgnoreCase("null")
                ||MeetinRoomTypeSelected.equalsIgnoreCase("null")||dateSelected.equalsIgnoreCase("null")||timeSelected.equalsIgnoreCase("null")){
            errormsgOfValidaationProceedButton.setVisibility(View.VISIBLE);
          //  proceedButton.setBackgroundColor(Color.parseColor("#cccccc"));
            isActivatePayButton="false";

        }else {
            try {
                isActivatePayButton="true";
                JSONObject savedObj = null;
                    savedObj = new JSONObject(readFromFile(BookMeetingRoom.this));
                    if (savedObj.has("MobNo")) {
                        String mobNo = savedObj.getString("MobNo");
                        String token = savedObj.getString("token");
                        mobile_text.setText("+91 "+mobNo);

                        changeNumberButtonLayout.setVisibility(View.VISIBLE);
                        DetailsOfUser.MobileNumber = Long.parseLong(mobNo);
                        DetailsOfUser.setAuthToken(token);

                        proceedToLastStep(mobNo);

                    } else {
                        isActivatePayButton="false";
                        changeNumberButtonLayout.setVisibility(View.GONE);
                        generateOtpCodeLayout.setVisibility(View.VISIBLE);
                        fname_editbox.setVisibility(View.GONE);
                        email_editbox.setVisibility(View.GONE);

                        payNowButton.setBackgroundColor(Color.parseColor("#cccccc"));


                    }


                proceedButton.setBackgroundColor(Color.parseColor("#f55416"));
                errormsgOfValidaationProceedButton.setVisibility(View.GONE);
                linearLayoutToHide.setVisibility(View.GONE);
                yourReqirementText.setText("Requirements");
                yourReqirementText.setTransformationMethod(null);
                yourReqirementText.setTextColor(Color.parseColor("#D5D5D5"));
                yourReqmtDetails.setVisibility(View.VISIBLE);
                confirmAndPay.setTextColor(Color.parseColor("#f55416"));
                linearSubCheckandPay.setVisibility(View.VISIBLE);
                //for open
                if (dateButton.getText().toString().equalsIgnoreCase("Select Date")) {
                    yourReqmtDetails.setText("Select Date " + " , " + "No Duration");
                } else {
                    yourReqmtDetails.setText(dateSelected  );
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    // SANCHIT STEP #4

    public  void proceedToLastStep(String mobno){
          CommonUtil.getAllCouponCodes(mobno);


    }



    public void validationForCheckAndPay() {
        if(citySelected.equalsIgnoreCase("null")){
            linearLayoutSelectCity.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else {
            linearLayoutSelectCity.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(centerSelected.equalsIgnoreCase("null")){
            linearLayoutSelectCenter.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else{
            linearLayoutSelectCenter.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(MeetinRoomTypeSelected.equalsIgnoreCase("null")){
            linearSubMeetingType.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else{
            linearSubMeetingType.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if(dateSelected.equalsIgnoreCase("null")){
            dateView.setBackgroundResource(R.drawable.transparent_bg_corner_red);
        }else{
            dateView.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        }

        if (fname_editbox.getText().toString().equalsIgnoreCase("") || fname_editbox.getText().toString().equalsIgnoreCase(" ")) {
            fname_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_red);
            errormsgName.setVisibility(View.VISIBLE);
            nameEntered = "null";

        } else {
            fname_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
            errormsgName.setVisibility(View.GONE);
            nameEntered = "true";
            DetailsOfUser.userName=fname_editbox.getText().toString();
        }
        if (email_editbox.getText().toString().equalsIgnoreCase("") || !isEmailValid(email_editbox.getText().toString().trim())) {
            email_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_red);
            errormsgEmail.setVisibility(View.VISIBLE);
            if(email_editbox.getText().toString().equalsIgnoreCase("") ) {
                errormsgEmail.setText("Please Enter Your Email");
            }else{
                errormsgEmail.setText("Please Enter A Valid Email Id");
            }
            emailEntered = "null";
        } else {
            email_editbox.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
            errormsgEmail.setVisibility(View.GONE);
            emailEntered = "true";
            DetailsOfUser.userName=email_editbox.getText().toString();
        }


        if (citySelected.equalsIgnoreCase("null")|| centerSelected.equalsIgnoreCase("null")||MeetinRoomTypeSelected.equalsIgnoreCase("null")||
                nameEntered.equalsIgnoreCase("null") || emailEntered.equalsIgnoreCase("null")) {
            Toast.makeText(BookMeetingRoom.this, "Fields Cannot be empty", Toast.LENGTH_SHORT).show();
        } else {
            CommonUtil.bookingApi("payumoney");
            Toast.makeText(BookMeetingRoom.this, "Please Wait .. Taking to your transaction", Toast.LENGTH_SHORT).show();
        }
    }


    public void resetValueForAll(){
        expListView.setVisibility(View.GONE);
        selectCenter.setVisibility(View.VISIBLE);
        centerSelected="null";
        selectCenter.setText("SELECT YOUR CENTER");
        ceneterName.setVisibility(View.GONE);
        resetValuesForRequirement();
    }

    public  void linear2function(){
        if(activeLinear.equalsIgnoreCase("linearSelectCity")){
            if(expListView.getVisibility()==View.VISIBLE){
                expListView.setVisibility(View.GONE);
                selectcityText.setVisibility(View.VISIBLE);
                arrowImgselectcity.setVisibility(View.VISIBLE);
                cityTextName.setVisibility(View.VISIBLE);

            }
        }
        activeLinear = "linearSelectCenter";
        expListView = (ExpandableListView) findViewById(R.id.expandableSelectCenter);
        expListView.setVisibility(View.VISIBLE);
        ceneterName.setVisibility(View.GONE);
        selectCenter.setVisibility(View.GONE);
        arrowImgselectCenter.setVisibility(View.GONE);
        startExpandables();
        expListView.expandGroup(0);
        linearLayoutToHide.setVisibility(View.GONE);
        previousCitySelected=citySelected;
    }

    public void linearSelectCityfunction(){
        if(activeLinear.equalsIgnoreCase("linearSelectCenter")){
            if(expListView.getVisibility()==View.VISIBLE){
                expListView.setVisibility(View.GONE);
                if(centerSelected.equalsIgnoreCase("null")) {
                    selectCenter.setVisibility(View.VISIBLE);
                    selectCenter.setText("SELECT YOUR CENTER");
                    arrowImgselectCenter.setVisibility(View.VISIBLE);
                }else{
                    selectCenter.setVisibility(View.VISIBLE);
                    ceneterName.setVisibility(View.VISIBLE);
                    arrowImgselectCenter.setVisibility(View.VISIBLE);
                }
            }
        }


        activeLinear="linearSelectCity";
        expListView = (ExpandableListView) findViewById(R.id.expandableSelectCity);
        expListView.setVisibility(View.VISIBLE);
        selectcityText.setVisibility(View.GONE);
        arrowImgselectcity.setVisibility(View.GONE);
        cityTextName.setVisibility(View.GONE);
        startExpandables();
        expListView.expandGroup(0);
        linearLayoutToHide.setVisibility(View.GONE);
    }


    private void openYourRequiremnt() {
        yourReqirementText.setText("YOUR REQUIREMENTS");
        yourReqirementText.setPadding(30, 6, 0, 12);
        yourReqirementText.setTextColor(Color.parseColor("#f55416"));
        yourReqmtDetails.setVisibility(View.GONE);
        requirmentSubLinear.setVisibility(View.VISIBLE);
        linearLayoutToHide.setVisibility(View.VISIBLE);
    }

    public void resetValuesForRequirement(){
        //for requirement
        yourReqirementText.setText("YOUR REQUIREMENTS");
        yourReqirementText.setTextColor(Color.parseColor("#C8C7C6"));
        yourReqmtDetails.setVisibility(View.GONE);
        requirmentSubLinear.setVisibility(View.GONE);
        linearLayoutToHide.setVisibility(View.GONE);


        //for meeting room type
        meetingroomTypeText.setVisibility(View.VISIBLE);
        meetingroomTypeText.setTextColor(Color.parseColor("#e11d1d1d"));
        arrowImageRequiremnt.setVisibility(View.VISIBLE);
        MeetinRoomTypeSelected="null";
       // selectedMeetingRoomType.setVisibility(View.GONE);

        //for date
        dateView.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
        dateButton.setText("Date");
        dateButton.setTextColor(Color.parseColor("#e11d1d1d"));
        dateSelected="null";


        linearButtonOfTime.setVisibility(View.GONE);
        linearCheckboxes.setVisibility(View.GONE);



        //for Confirm And Pay
        confirmAndPay.setTextColor(Color.parseColor("#C8C7C6"));
        linearSubCheckandPay.setVisibility(View.GONE);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public void getCostOfTime(View v){
        int indexNo = Integer.parseInt(v.getTag().toString());
      //  Toast.makeText(this, "getCostOfTime "+indexNo, Toast.LENGTH_LONG).show();
        CommonUtil.updateToAndFromTime(indexNo);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        meetingroomTypeText.setVisibility(View.GONE);
        arrowImageRequiremnt.setVisibility(View.GONE);
        CommonUtil.CheckAndCallSchedulesForMeetingRoomType(i);
        MeetinRoomTypeSelected=adapterView.getItemAtPosition(i).toString();

        //CommonUtil.resetAllButtonsOnDateCahanged();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void sendOtp(){

       // long mob = 9864903840L;
        long mob = Long.parseLong(mobile_editbox.getText().toString());
        DetailsOfUser.MobileNumber=mob;

        CommonUtil.sendOtp(mob, new Callback<HashMap>() {

            @Override
            public void success(HashMap res, Response response) {
               // Toast.makeText(getApplicationContext(),"OTP SENT SUCCESSFULLY",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                //Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
            }

        });

    };

    private void verifyOtp(){

       // long mob = 9205627721L;
        long mob = DetailsOfUser.MobileNumber;

        //int otp = 123456;
        int otp = Integer.parseInt(otpCodeEditBox.getText().toString());

        CommonUtil.verifyOtp(mob,otp,new Callback<HashMap>(){

            @Override
            public void success(HashMap res, Response response) {
               // Toast.makeText(getApplicationContext(),"OTP VERIFIED SUCCESSFULLY",Toast.LENGTH_SHORT).show();
                    try {
                        payNowButton.setBackgroundColor(Color.parseColor("#f55416"));
                        fname_editbox.setVisibility(View.VISIBLE);
                        email_editbox.setVisibility(View.VISIBLE);
                        submitOtpLayout.setVisibility(View.GONE);
                        isActivatePayButton="true";
                        showNameAndEmailEditBox(true);
                        JSONObject obj = new JSONObject();
                        obj.put("MobNo", DetailsOfUser.MobileNumber);
                        obj.put("token","Bearer f3915eae-dc6c-4808-a831-457384ee3b72");
                        writeToFile(obj.toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }

            };

            @Override
            public void failure(RetrofitError error) {
               // Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();

                otpCodeEditBox.setBackgroundResource(R.drawable.transparent_bg_corner_red);
                errorMsgofOtp.setVisibility(View.VISIBLE);
            };

        });
    };

    public static void initializeSpinner(Activity act){

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(act, android.R.layout.simple_spinner_item,CommonUtil.meetingRoomTypeNametoDisplay);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        meetingSpinner.setAdapter(dataAdapter);
        //meetingSpinner.setOnItemSelectedListener();
    }

    public void showNameAndEmailEditBox(boolean show){
        if(show){
            fname_editbox.setVisibility(View.VISIBLE);
            email_editbox.setVisibility(View.VISIBLE);
            payNowButton.setBackgroundColor(Color.parseColor("#f55416"));
        }else{
            fname_editbox.setVisibility(View.GONE);
            errormsgName.setVisibility(View.GONE);
            email_editbox.setVisibility(View.GONE);
            errormsgEmail.setVisibility(View.GONE);
            payNowButton.setBackgroundColor(Color.parseColor("#cccccc"));
        }
    }

    public  void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getApplicationContext().openFileOutput("IOData.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile(Context context) {
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("IOData.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String receiveString = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((receiveString = bufferedReader.readLine()) != null) {
                stringBuilder.append(receiveString);
            }
            inputStream.close();
            ret = stringBuilder.toString();
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
            e.printStackTrace();
        }
        return ret;
    }

    @Override
    public void onBackPressed() {

        ScrollView sv= (ScrollView)findViewById(R.id.mainScrollableview);
        if(sv.getVisibility()==View.GONE){
            sv.setVisibility(View.VISIBLE);
        }else{
            this.finish();
            super.onBackPressed();
        }
    }
}


