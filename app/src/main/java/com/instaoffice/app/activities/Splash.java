package com.instaoffice.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.instaoffice.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class Splash extends AppCompatActivity {

    SharedPreferences prefs = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for setting fullScreen Method
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//to check first run
        prefs = getSharedPreferences("com.instaoffice.app", MODE_PRIVATE);
        setContentView(R.layout.splash);

    }

    @Override
    protected void onResume() {
        super.onResume();



        if (prefs.getBoolean("firstrun", true)) {

            //LoginManager.getInstance().logOut();
            JSONObject obj = new JSONObject();
            try {
                obj.put("Hi", "Hello");
                writeToFile(obj.toString());
                // Toast.makeText(this, "File Saved To DiD null ", Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                e.printStackTrace();
                // Toast.makeText(this, "EXCEPTION ", Toast.LENGTH_SHORT).show();
            }
            prefs.edit().putBoolean("firstrun", false).commit();
        }

        new CountDownTimer(2000,1000){
            @Override
            public void onTick(long millisUntilFinished){}

            @Override
            public void onFinish(){
                //set the new Content of your activity
                goTOBookMeetinRoomActivity();

            }
        }.start();
    }
    void goTOBookMeetinRoomActivity(){
        Intent intent=new Intent(this,BookMeetingRoom.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right,R.anim.slide_out_to_left);
    }

    public  void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getApplicationContext().openFileOutput("IOData.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
