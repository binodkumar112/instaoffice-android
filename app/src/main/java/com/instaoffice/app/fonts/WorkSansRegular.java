package com.instaoffice.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by SANCHIT on 9/17/2016.
 */

    public class WorkSansRegular extends TextView {

        public WorkSansRegular(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public WorkSansRegular(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public WorkSansRegular(Context context) {
            super(context);
            init();
        }

        private void init() {
            //if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-Regular.otf");
            setTypeface(tf);
            //}
        }

    }
