package com.instaoffice.app.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;


import com.instaoffice.app.R;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by SANCHIT on 1/28/2017.
 */

public class Navigation_frag extends android.support.v4.app.Fragment implements View.OnClickListener {


    private DrawerLayout mDrawerLayout;
    Context context;
    private View containerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private NavigationDrawerCallbacks mCallbacks;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();




        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        RelativeLayout mDrawerScrollView = (RelativeLayout) inflater.inflate(R.layout.navigation_menu, container, false);
        return  mDrawerScrollView;





    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.dummy_button, R.string.dummy_content) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawer(containerView);
        int id = view.getId();
        switch (id) {
           case R.id.navigation_profiletext:
                if(mCallbacks!=null)
                  //  mCallbacks.goToProfilePage();
                break;

         /*   case R.id.navigation_sharetext:
                if(mCallbacks!=null)
                    mCallbacks.goToSharefunction();
                break;
            case R.id.navigation_helptext:
                if(mCallbacks!=null)
                    mCallbacks.goTohelpFunction();
                break;

            case R.id.navigation_settingtext:
                if(mCallbacks!=null)
                    mCallbacks.goToSettingPage();
                break;
            case R.id.navigation_contacttext:
                //  if(mCallbacks!=null)
                //  mCallbacks.goToContactPage();
                break;
            case R.id.navigation_rateutext:
                if(mCallbacks!=null)
                    // mCallbacks.goToSettingPage();
                    break;
            case R.id.profileImg:
                if(mCallbacks!=null)
                    mCallbacks.goToProfilePage();
                break;
            case R.id.settingImg:
                if(mCallbacks!=null)
                    mCallbacks.goToSettingPage();
                break;
            case R.id.contactUsImg:
                if(mCallbacks!=null)
                    mCallbacks.goTohelpFunction();
                break;
            case R.id.shareImg:
                if(mCallbacks!=null)
                    mCallbacks.goToSharefunction();
                break;
*/
            default:
                throw new UnsupportedOperationException("Id not found " + id);
        }
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */

      //  void goToSettingPage();

       // void goToProfilePage();

        // void goToContactPage();


    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
    }

    private void openDrawer() {
        mDrawerLayout.openDrawer(Gravity.RIGHT);
    }




}