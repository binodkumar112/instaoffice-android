package com.instaoffice.app.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.instaoffice.app.R;
import com.instaoffice.app.activities.BookMeetingRoom;
import com.instaoffice.app.models.DetailsOfUser;
import com.instaoffice.app.models.CommonModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
public class CommonUtil {


    // MOBILE VERIFICATION
    public static long mobileNumber;
    public static int seqid = 12345;





    public static Activity activity;

    //constructor
    public CommonUtil(Activity act) {
        this.activity = act;
    }

    public static int a[] = {R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9};
    public static int btnText[] = {R.id.btn1Txt, R.id.btn2Txt, R.id.btn3Txt, R.id.btn4Txt, R.id.btn5Txt, R.id.btn6Txt, R.id.btn7Txt, R.id.btn8Txt, R.id.btn9Txt};
    public static int btnLine[] = {R.id.btn1Line, R.id.btn2Line, R.id.btn3Line, R.id.btn4Line, R.id.btn5Line, R.id.btn6Line, R.id.btn7Line, R.id.btn8Line, R.id.btn9Line};

    //Assignments
    public static List<String> cityNametoDisplay = new ArrayList<String>();
    public static List<String> centerNametoDisplay = new ArrayList<String>();
    public static List<String> propertyIdOfTheSelectedCenter = new ArrayList<String>();
    public static List<String> meetingRoomTypeNametoDisplay = new ArrayList<String>();
    public static List<String> meetingRoomresourceCode = new ArrayList<String>();
    public static List<String> timeListToDisplay = new ArrayList<String>();

    public static JSONObject idToBeBlockedInJsonFormat;

    public static List<Integer> BookedSlotsnumber = new ArrayList<Integer>();
    public static List<Integer> SlotsIndexBooked = new ArrayList<Integer>();
    public static List<Integer> buttonIdsToColred;


    public static String citySelectedByUser;
    public static String centerSelectedByUser;
    public static String meetingRoomSelectedByUser;
    public static JSONObject propertydataforAllcities = new JSONObject();
    public static JSONObject centerdataforAllcities = new JSONObject();


    DetailsOfUser dtu = new DetailsOfUser();

    // for yhe list of cities API Call
    public void getCitiesListthroughApi() {
        try {
            final ProgressDialog loading = ProgressDialog.show(activity, null, "      Please wait...", false, false);

            System.out.println("----------------------------------------------------" + "GetCitiesApi");
            RestClient.get().getListOfCities(new Callback<ArrayList>() {
                @Override
                public void success(ArrayList res, Response response) {
                    CommonModel loc = new CommonModel();
                    loc.setListOfCities(res);
                    cityNametoDisplay.clear();
                    JSONObject obj = null;
                    for (int i = 0; i < res.size(); i++) {
                        try {
                            obj = new JSONObject((Map) res.get(i));
                            cityNametoDisplay.add(obj.getString("display"));
                            System.out.println("----------------------------------------------------" + obj.getString("display"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    loading.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("--------------------------------------error--------------" + error);
                    loading.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("----------------------------------------------------" + e);
        }
    }

    // for the centers API Call
    public static void getSubListOfCitiesSelectedByUserthroughApi(final String code) {
        try {
            final ProgressDialog loading = ProgressDialog.show(activity, null, "      Please wait...", false, false);
            RestClient.get().getSubListOfCitiesSelected(code, new Callback<ArrayList>() {

                @Override
                public void success(ArrayList res, Response response) {
                    try {
                        CommonModel loc = new CommonModel();
                        loc.setListOfCenters(res);

                        System.out.println("-------------------------------------------------------" + res);
                        centerNametoDisplay.clear();
                        propertyIdOfTheSelectedCenter.clear();
                        JSONObject obj = null;
                        for (int i = 0; i < res.size(); i++) {
                            obj = new JSONObject((Map) res.get(i));
                            centerNametoDisplay.add(obj.getString("filename"));
                            propertyIdOfTheSelectedCenter.add(obj.getString("id"));

                            System.out.println("----------------------------------------------------" + obj.getString("filename"));
                        }
                        loading.dismiss();
                        propertydataforAllcities.put(code, new JSONArray(res));
                        System.out.println("------------------------------------------------------123-" + propertydataforAllcities);
                        //  bmr.linear2function();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("------------------------------------------------------err-" + error);
                    loading.dismiss();
                }
            });
            System.out.println("----------------------------------------------------" + "getSubListOfCitiesSelected");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //for meeting Type API Call
    public static void getTheMeetingRoomTypeThroughApi(final String propId) {
        try {
            final ProgressDialog loading = ProgressDialog.show(activity, null, "      Please wait...", false, false);
            RestClient.get().getMeetingRoomType(propId, new Callback<ArrayList>() {
                @Override
                public void success(ArrayList res, Response response) {
                    try {
                        System.out.println("=========================================" + res.size() + "==========" + res);
                        CommonModel.setCheckBoxDatatodisplay(res);
                        meetingRoomTypeNametoDisplay.clear();
                        meetingRoomresourceCode.clear();
                        JSONObject obj = null;
                        for (int i = 0; i < res.size(); i++) {
                            obj = new JSONObject((Map) res.get(i));
                            JSONObject payloadObj = new JSONObject(obj.getString("payload"));
                            System.out.println("======================================length of payload --> " + payloadObj.length());
                            meetingRoomresourceCode.add(obj.getString("resourcecode"));
                            meetingRoomTypeNametoDisplay.add(payloadObj.getString("display"));
                            System.out.println("======================================" + obj.getString("resourcecode"));
                        }
                        loading.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    BookMeetingRoom.initializeSpinner(activity);
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("========================================================err-" + error);

                    loading.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //To Call Api Shedule
    public static void getScheduleAfterDateSelection(final String fromMilisec, final String toMilisec) {
        try {
            System.out.println("---------------------------------------------------------1487215800000--" + fromMilisec);
            System.out.println("---------------------------------------------------------1487248200000--" + toMilisec);
            final ProgressDialog loading = ProgressDialog.show(activity, null, "      Please wait...", false, false);
            RestClient.get().getShedule(DetailsOfUser.getPropertyId(), DetailsOfUser.getMeetingRoomType(), fromMilisec, toMilisec, new Callback<ArrayList>() {
                @Override
                public void success(ArrayList res, Response response) {
                    System.out.println("--------------------------------------------schedule---------------" + res.size());
                    JSONObject scheduleObj = new JSONObject();

                    try {
                        LinearLayout buttnlayout = (LinearLayout) activity.findViewById(R.id.toDisplayButton);
                        buttnlayout.setVisibility(View.VISIBLE);

                        BookedSlotsnumber = new ArrayList<Integer>();
                        JSONObject obj = null;
                        idToBeBlockedInJsonFormat = new JSONObject();

                        for (int y = 0; y < 9; y++) {
                            idToBeBlockedInJsonFormat.put(y + "", "true");
                        }

                        for (int i = 0; i < res.size(); i++) {
                            obj = new JSONObject(String.valueOf(res.get(i)));
                            Long fromdatebooked = obj.getLong("fromdate");
                            Long todatebooked = obj.getLong("todate");

                            int numberOfSlots = getNumberOfSlots(fromdatebooked, todatebooked);
                            int slotIndex = getStartingSlotIndex(Long.valueOf(fromMilisec), fromdatebooked);
                            // int slotIndex = getStartingSlotIndex(Long.valueOf("1487203201000"), fromdatebooked);
                            System.out.println("---------------------------------------------------------------" + numberOfSlots + "kkkk" + slotIndex);
                            //SlotsIndexBooked.add(numberOfSlots);
                            BookedSlotsnumber.add(slotIndex);
                            displayButton(numberOfSlots,slotIndex);
                            //  idToBeBlockedInJsonFormat.put(nos+"","false");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loading.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                   // updateCostOfRoom("0");
                    loading.dismiss();
                   // Toast.makeText(activity, "Resource Not Free", Toast.LENGTH_SHORT).show();
                    System.out.println("-----------------------------------------------------error in schedule------" + error);
                    //loading.setMessage("Error in Geeting Data in Schedule");

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //to display the time buttons
    public static void displayButton(int numberOfSlots, int startingIndex) {


        for (int x = startingIndex; x < numberOfSlots; x++) {
            //Button btn = (Button) activity.findViewById(a[x]);
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[x]);
            lineralayourButton.setBackgroundColor(Color.parseColor("#d6d5d2"));
            lineralayourButton.setEnabled(false);

        }
    }


    private static int getNumberOfSlots(Long fromdatebooking, Long todatebooking) {

        int slotsNumber = 0;
        int db = 3600000;

        slotsNumber = (int) ((todatebooking - fromdatebooking) / db);

        return slotsNumber;
    }

    ;

    private static int getStartingSlotIndex(Long startingVal, Long fromdatebooking) {

        int slotsNumber = 0;
        int db = 3600000;

        slotsNumber = (int) ((fromdatebooking - startingVal) / db);

        return slotsNumber;
    }

    ;

    // Automatically detect to call Api or not
    public static void CheckAndCallApiForSubListOfCities(int position) {
        try {
            JSONObject obj = new JSONObject((Map) CommonModel.listOfCities.get(position));
            citySelectedByUser = obj.getString("code");

            DetailsOfUser dtu = new DetailsOfUser();
            dtu.setCityCode(citySelectedByUser);
            CommonModel loc = new CommonModel();

            centerNametoDisplay.clear();
            propertyIdOfTheSelectedCenter.clear();
            CommonUtil.getSubListOfCitiesSelectedByUserthroughApi(citySelectedByUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // for calling the Api
    public static void checkAndCallApiForMeetingType(int position) {
        try {
            String prpId = propertyIdOfTheSelectedCenter.get(position);
            DetailsOfUser dtu = new DetailsOfUser();
            dtu.setPropertyId(prpId);
            getTheMeetingRoomTypeThroughApi(prpId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //for getting the Schedule of The Meeting Room Type Selected
    public static void CheckAndCallSchedulesForMeetingRoomType(int position) {
        String selectedRoommType = meetingRoomresourceCode.get(position);
        DetailsOfUser.meetingRoomResourceCode = selectedRoommType;
        try {
            JSONObject obj = new JSONObject((Map) CommonModel.getCheckBoxDatatodisplay().get(position));
            JSONObject newObj = obj.getJSONObject("payload");
            updateCostOfRoom("0");
            JSONArray amenitiesdata = new JSONArray(newObj.getString("amenities"));
            //for(int i=0;i<amenitiesdata.length();i++){
            // do something for        }
            resetAllButtonsOnDateCahanged();
            displayCheckBox(amenitiesdata.length(), amenitiesdata);
            if(DetailsOfUser.milliSecondsOfStart!=0){
                getScheduleAfterDateSelection(String.valueOf(DetailsOfUser.milliSecondsOfStart),String.valueOf(DetailsOfUser.milliSecondsOfEnd));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayCheckBox(int noOfChecksBoxes, JSONArray data) {
        try {
            DetailsOfUser.amenotiestype = new ArrayList();
            LinearLayout ll = (LinearLayout) activity.findViewById(R.id.linearcheckboxes);

            ll.removeAllViews();
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.leftMargin = 123;
            DetailsOfUser.aminetiesSelectedByUser = new JSONObject();
            DetailsOfUser.am = new JSONArray();
            Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/WorkSans-Medium.otf");
            //add checkboxes
            JSONObject obj = null;
            for (int i = 0; i < noOfChecksBoxes; i++) {
                obj = new JSONObject(data.get(i).toString());
                int pricofAminety = obj.getInt("amenityprice");
                final AppCompatCheckBox cb = new AppCompatCheckBox(activity);
                cb.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                cb.setText(obj.getString("amenitydisplay"));
                cb.setTypeface(typeface);
                cb.setId(i);
                cb.setHighlightColor(Color.parseColor("#f55416"));
                cb.setChecked(true);
                cb.setTextSize(13);
                params.setMargins(12, 2, 20, 2);

                ll.addView(cb, params);
                String aminetyType = obj.getString("amenitytype");

                //clearing the values
                DetailsOfUser.am.put(aminetyType);
                DetailsOfUser.amenotiestype.add(aminetyType);
                System.out.println("=========================================123==" + DetailsOfUser.amenotiestype);
                System.out.println("=========================================JsonArray==" + DetailsOfUser.amenotiestype);
                SetOnClickForCheckBoxes(activity, cb, i, pricofAminety, aminetyType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void SetOnClickForCheckBoxes(final Activity act, final CheckBox cb, final int i, int priceOfAminety, final String aminetyType) {

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                try {
                    System.out.println("====================is checked=" + cb.isChecked());
                    JSONObject nobj = new JSONObject();
                    if (cb.isChecked()) {
                        System.out.println("====================cb.isChecked()=" + cb.isChecked());
                        DetailsOfUser.am.put(aminetyType);
                    } else {
                        System.out.println("====================detaails " + DetailsOfUser.am.length());
                        System.out.println("====================int " + i);
                        if (DetailsOfUser.am.length() > 1) {
                            DetailsOfUser.am.remove(i);

                        } else {
                            DetailsOfUser.am = new JSONArray();
                        }
                    }
                    System.out.println("====================crreent=" + DetailsOfUser.am);
                    getPriceForTheTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public static int fromTime = -1;
    public static int toTime = -1;
    public static boolean isContinious = true;

    //to get The time
    public static void updateToAndFromTime(int index) {

        if (fromTime == -1) {
            updateCostOfRoom("0");
            buttonIdsToColred = new ArrayList<Integer>();
            buttonIdsToColred.add(index);
            fromTime = index;
            addColorToButton();
        } else if (toTime == -1) {
            Toast.makeText(activity, "isContiniousTimeAvailable(index) "+ isContiniousTimeAvailable(index), Toast.LENGTH_SHORT).show();
            if (isContiniousTimeAvailable(index)){
                if (fromTime != index && fromTime < index) {
                    toTime = index;
                    // 7 to array
                    buttonIdsToColred.add(index);
                    addSlots(index);
                    addColorToButton();
                }else if(fromTime != index && fromTime > index){
                    fromTime = index;
                    buttonIdsToColred = new ArrayList<Integer>();
                    updateCostOfRoom("0");
                    buttonIdsToColred.add(index);
                    addColorToButton();
                }else {
                    fromTime = -1;
                    buttonIdsToColred = new ArrayList<Integer>();
                    updateCostOfRoom("0");
                    addColorToButton();
                }
            } else {
                fromTime = index;
                buttonIdsToColred = new ArrayList<Integer>();
                buttonIdsToColred.add(index);
                addColorToButton();
                toTime = -1;
                Toast.makeText(activity, "last second  "+index, Toast.LENGTH_SHORT).show();
            }
        }else{
            fromTime = index;
            buttonIdsToColred = new ArrayList<Integer>();
            buttonIdsToColred.add(index);
            addColorToButton();
            toTime = -1;
            Toast.makeText(activity, "In Time Function Else  "+index, Toast.LENGTH_SHORT).show();
        }
        if(fromTime!=-1) {
            calculateTheTimeAndCallApi();
        }
    }


    public static boolean isContiniousTimeAvailable(int index) {
        if(fromTime != index) {
            for (int i = fromTime; i < index; i++) {
                if (BookedSlotsnumber.contains(i)) {
                    isContinious = false;
                    break;
                }
                return isContinious;
            }
        }
        return true;
    }

    //to calculate The from And To Date
    public static void calculateTheTimeAndCallApi() {

        DetailsOfUser.startTimeOfBooking = fromTime * 3600000 + DetailsOfUser.milliSecondsOfStart;
        if (toTime == -1) {
            DetailsOfUser.endTimeOfBooking = 1 * 3600000 + DetailsOfUser.startTimeOfBooking;
            System.out.println("-----------------------------------------------------to timee =-1  ------" + DetailsOfUser.startTimeOfBooking);
        } else {
            DetailsOfUser.endTimeOfBooking = (toTime* 3600000 + 3600000) + DetailsOfUser.milliSecondsOfStart;
            System.out.println("-------------------to time "+toTime+"----------------------------------------" + DetailsOfUser.startTimeOfBooking);
        }

        System.out.println("------------------------------------------------------------" + DetailsOfUser.startTimeOfBooking);
        System.out.println("------------------------------------------------------------" + DetailsOfUser.endTimeOfBooking);

        getPriceForTheTime();
        LinearLayout linearButtonOfTime=(LinearLayout) activity.findViewById(R.id.toDisplayButton);
        linearButtonOfTime.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
    }

    //for getting the Price of the room API CALLING
    public static void getPriceForTheTime() {
        try {
            DetailsOfUser.aminetiesSelectedByUser = new JSONObject();
            DetailsOfUser.aminetiesSelectedByUser.put("amenities", DetailsOfUser.am);
            final ProgressDialog loading = ProgressDialog.show(activity, null, "      Please wait...", false, false);
            System.out.println("-----------------------------------------------------newObj" + DetailsOfUser.aminetiesSelectedByUser);
            System.out.println("-------------------------------------------------startTime" + String.valueOf(DetailsOfUser.startTimeOfBooking));
            System.out.println("-----------------------------------------------------EndTime" + String.valueOf(DetailsOfUser.endTimeOfBooking));

            RestClient.get().getPriceForTheSpecificTime(String.valueOf(DetailsOfUser.MobileNumber), String.valueOf(DetailsOfUser.startTimeOfBooking), String.valueOf(DetailsOfUser.endTimeOfBooking), DetailsOfUser.getPropertyId(), DetailsOfUser.meetingRoomResourceCode, "code", DetailsOfUser.aminetiesSelectedByUser, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    try {
                        JSONObject obj = new JSONObject(res);
                        System.out.println("-----------------------------------------------------successInGetPriceForTheTime" + obj.toString());
                        updateCostOfRoom(String.valueOf(obj.getInt("amount")));
                        DetailsOfUser.setTotalcostOfUsersRoom(obj.getInt("amount"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loading.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("-----------------------------------------------------errorInGetPriceForTheTime" + error);
                    updateCostOfRoom("0");
                    Toast.makeText(activity, "Resource Not Free", Toast.LENGTH_SHORT).show();
                    fromTime=-1;
                    BookMeetingRoom.timeSelected="null";
                    resetButtonsOnResourceNotFree();

                    loading.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //get Api for Coupon
    public static void getAllCouponCodes(String mobno) {
        try {
            RestClient.get().getAllCouponsForTheUser("Bearer f3915eae-dc6c-4808-a831-457384ee3b72",mobno, new Callback<ArrayList>() {
                @Override
                public void success(ArrayList res, Response response) {
                    System.out.println("-----------------------------------------------------------------success " + res);
                    JSONObject obj = null;
                    DetailsOfUser.CouponsForUser = new ArrayList();
                   // Toast.makeText(activity, "Success in All Coupon", Toast.LENGTH_SHORT).show();
                 //   for (int i = 0; i < res.size(); i++) {
                    try {

                        obj = new JSONObject((Map) res.get(0));
                            DetailsOfUser.setWalletBalanceOfUser(obj.getInt("amount"));
                          // DetailsOfUser.setWalletBalanceOfUser(100);
                            if(DetailsOfUser.getWalletBalanceOfUser()>=DetailsOfUser.getTotalcostOfUsersRoom()){
                                showWalletwithAmount(true);
                            }else{
                               showWalletwithAmount(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }

                @Override
                public void failure(RetrofitError error) {
                  //  Toast.makeText(activity, "FAiled in Coupon", Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateCostOfRoom(String cost) {
        TextView totalcost = (TextView) activity.findViewById(R.id.rstext);
        totalcost.setText("\u20B9" + " " + cost);
    }

    //initiate A Booking
    public static void bookingApi(String paymentType) {
        try {
            System.out.println("=========================================================initiateaBookingForTheRoom function ");
            System.out.println("=========================================================DetailsOfUser.getPropertyId() " + DetailsOfUser.getPropertyId());
            System.out.println("=========================================================DetailsOfUser.meetingRoomResourceCode " + DetailsOfUser.meetingRoomResourceCode);
            DetailsOfUser.aminetiesSelectedByUser = new JSONObject();
            DetailsOfUser.aminetiesSelectedByUser.put("amenities", DetailsOfUser.am);
            RestClient.get().initiateaBookingForTheRoom("Bearer f3915eae-dc6c-4808-a831-457384ee3b72",String.valueOf(DetailsOfUser.MobileNumber), String.valueOf(DetailsOfUser.startTimeOfBooking), String.valueOf(DetailsOfUser.endTimeOfBooking), DetailsOfUser.getPropertyId(), DetailsOfUser.meetingRoomResourceCode, paymentType, DetailsOfUser.aminetiesSelectedByUser, new Callback<HashMap>() {
           // RestClient.get().initiateaBookingForTheRoom("Bearer f3915eae-dc6c-4808-a831-457384ee3b72","9819014845", String.valueOf(DetailsOfUser.startTimeOfBooking), String.valueOf(DetailsOfUser.endTimeOfBooking), DetailsOfUser.getPropertyId(), DetailsOfUser.meetingRoomResourceCode, paymentType, DetailsOfUser.aminetiesSelectedByUser, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    System.out.println("=========================================================initiateaBookingForTheRoom" + res);
                //    Toast.makeText(activity, "Suuccesssss", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("=========================================================initiateaBookingForTheRoom error" + error);
                 //   Toast.makeText(activity, "Failed initiate A Booking", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addSlots(int index) {

        if(fromTime == -1){
            buttonIdsToColred = new ArrayList<Integer>();
        }else{
            buttonIdsToColred = new ArrayList<Integer>();
            for(int i=fromTime;i<=index;i++){
                buttonIdsToColred.add(i);
            }
        }
    }

    public static void resetButtonsOnResourceNotFree(){
        for(int i=0;i<a.length;i++){
            // int b =  buttonIdsToColred.get(i);
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[i]);
            TextView textView=(TextView) activity.findViewById(btnText[i]);
            View lineofBtn=(View) activity.findViewById(btnLine[i]);
            if(buttonIdsToColred.contains(i)) {
                lineralayourButton.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                textView.setTextColor(Color.parseColor("#484848"));
                lineofBtn.setBackgroundColor(Color.parseColor("#484848"));
            }
        }

        for(int y=0;y<BookedSlotsnumber.size();y++){
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[BookedSlotsnumber.get(y)]);
            TextView textView=(TextView) activity.findViewById(btnText[BookedSlotsnumber.get(y)]);
            View lineofBtn=(View) activity.findViewById(btnLine[BookedSlotsnumber.get(y)]);
            lineralayourButton.setBackgroundColor(Color.parseColor("#d6d5d2"));
            textView.setTextColor(Color.parseColor("#484848"));
            lineofBtn.setBackgroundColor(Color.parseColor("#484848"));

        }
    }

    public static void addColorToButton(){
        for(int i=0;i<a.length;i++){
           // int b =  buttonIdsToColred.get(i);
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[i]);
            TextView textView=(TextView) activity.findViewById(btnText[i]);
            View lineofBtn=(View) activity.findViewById(btnLine[i]);
            if(buttonIdsToColred.contains(i)) {
               // LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[i]);
                lineralayourButton.setBackgroundColor(Color.parseColor("#f55416"));
                textView.setTextColor(Color.WHITE);
                lineofBtn.setBackgroundColor(Color.WHITE);
            }else{

                lineralayourButton.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
                textView.setTextColor(Color.parseColor("#484848"));
                lineofBtn.setBackgroundColor(Color.parseColor("#484848"));
            }
        }

        for(int y=0;y<BookedSlotsnumber.size();y++){
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[BookedSlotsnumber.get(y)]);
            TextView textView=(TextView) activity.findViewById(btnText[BookedSlotsnumber.get(y)]);
            View lineofBtn=(View) activity.findViewById(btnLine[BookedSlotsnumber.get(y)]);

            lineralayourButton.setBackgroundColor(Color.parseColor("#d6d5d2"));
            textView.setTextColor(Color.parseColor("#484848"));
            lineofBtn.setBackgroundColor(Color.parseColor("#484848"));

        }
    }

    public static void resetAllButtonsOnDateCahanged(){


        for(int i=0;i<=8;i++){
            LinearLayout lineralayourButton = (LinearLayout) activity.findViewById(a[i]);
            lineralayourButton.setBackgroundResource(R.drawable.transparent_bg_corner_grey);
            TextView textView=(TextView) activity.findViewById(btnText[i]);
            View lineofBtn=(View) activity.findViewById(btnLine[i]);
            textView.setTextColor(Color.parseColor("#484848"));
            lineofBtn.setBackgroundColor(Color.parseColor("#484848"));
            lineralayourButton.setEnabled(true);
        }
        toTime=-1;
        fromTime=-1;
        BookMeetingRoom.timeSelected="null";
    }

    public static void sendOtp(long mobileNumber, final Callback callback) {

        try {

            RestClient.get().sendOTP(mobileNumber, seqid, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    System.out.println("=========================================================initiateaBookingForTheRoom" + res);
                   // Toast.makeText(activity, "Suuccesssss", Toast.LENGTH_SHORT).show();
                    callback.success(res, response);
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("=========================================================initiateaBookingForTheRoom error" + error);
                 //   Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                    callback.failure(error);
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void verifyOtp(long mobileNumber, int otp, final Callback callback) {

        try {
            final ProgressDialog loading = ProgressDialog.show(activity, null, "    Verifying OTP....", false, false);
            RestClient.get().verifyOTP(mobileNumber, seqid, otp, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    System.out.println("=========================================================initiateaBookingForTheRoom" + res);
                 //   Toast.makeText(activity, "Suuccesssss", Toast.LENGTH_SHORT).show();
                    callback.success(res, response);
                    loading.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("=========================================================initiateaBookingForTheRoom error" + error);
                 //   Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                    callback.failure(error);

                    loading.dismiss();
                }

            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showWalletwithAmount(boolean mobilenoSaved){
        LinearLayout walletLayout =(LinearLayout) activity.findViewById(R.id.walletOfUser);
        TextView walletBalance= (TextView) activity.findViewById(R.id.walletBalance);
        TextView lowWalletBalance= (TextView) activity.findViewById(R.id.no_sufficentbalance);
        Button payWithWalletButton = (Button) activity.findViewById(R.id.payWithWalletButton);
        walletLayout.setVisibility(View.VISIBLE);
        walletBalance.setText("INR "+DetailsOfUser.walletBalanceOfUser);

        if(mobilenoSaved){
           // setPayUsingWallet();
            payWithWalletButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    InitiateTheBookingForWallet();
                }
            });
        }else{
            payWithWalletButton.setVisibility(View.GONE);
            lowWalletBalance.setVisibility(View.VISIBLE);
        }
    }



//initiate An bookin for Wallet
    public static void InitiateTheBookingForWallet() {
        try {
            DetailsOfUser.aminetiesSelectedByUser = new JSONObject();
            DetailsOfUser.aminetiesSelectedByUser.put("amenities", DetailsOfUser.am);
            System.out.println("------"+ String.valueOf(DetailsOfUser.MobileNumber)+"\n"+"--"+String.valueOf(DetailsOfUser.startTimeOfBooking)+"\n"+"----"+String.valueOf(DetailsOfUser.startTimeOfBooking+"\n"+"----"
                    +String.valueOf(DetailsOfUser.endTimeOfBooking+"----"+"\n"+ DetailsOfUser.getPropertyId())));

            RestClient.get().initiateaBookingForTheRoom("Bearer f3915eae-dc6c-4808-a831-457384ee3b72", String.valueOf(DetailsOfUser.MobileNumber), String.valueOf(DetailsOfUser.startTimeOfBooking), String.valueOf(DetailsOfUser.endTimeOfBooking), DetailsOfUser.getPropertyId(), DetailsOfUser.meetingRoomResourceCode, "code", DetailsOfUser.aminetiesSelectedByUser, new Callback<HashMap>() {
           // RestClient.get().initiateaBookingForTheRoom("Bearer f3915eae-dc6c-4808-a831-457384ee3b72","9819014845", String.valueOf(DetailsOfUser.startTimeOfBooking), String.valueOf(DetailsOfUser.endTimeOfBooking), DetailsOfUser.getPropertyId(), DetailsOfUser.meetingRoomResourceCode, "code", DetailsOfUser.aminetiesSelectedByUser, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    System.out.println("=========================================================initiateaBookingForTheRoom" + res);
                 //   Toast.makeText(activity, DetailsOfUser.MobileNumber+"Suuccesssss InitiateTheBookingForWallet", Toast.LENGTH_SHORT).show();
                  //  Toast.makeText(activity,res.toString(), Toast.LENGTH_SHORT).show();
                        try {
                            //get the the id in response and call another A API
                            JSONObject obj = new JSONObject(res);
                            String id = obj.getString("id");

                            deductCreditForWalletPayment(id);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                }
                @Override
                public void failure(RetrofitError error) {
                    System.out.println("=========================================================initiateaBookingForTheRoom error" + error);
                  //  Toast.makeText(activity, "Failed InitiateTheBookingForWallet", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void deductCreditForWalletPayment(String id){
        try {

            JSONObject obj=new JSONObject();
            obj.put("couponcode","c1234");

            System.out.println("------------------------"+obj.toString());


           RestClient.get().deductBalanceForPaymentMadeByWallet("Bearer f3915eae-dc6c-4808-a831-457384ee3b72",String.valueOf(DetailsOfUser.MobileNumber),obj.toString(),id, new Callback<HashMap>() {
          //  RestClient.get().deductBalanceForPaymentMadeByWallet("Bearer f3915eae-dc6c-4808-a831-457384ee3b72","9819014845",obj.toString(),id, new Callback<HashMap>() {
                @Override
                public void success(HashMap res, Response response) {
                    System.out.println("--"+res.toString());

                successMsg();
                }

                @Override
                public void failure(RetrofitError error) {
                   // Toast.makeText(activity, "FAiled in deductCreditForWalletPayment", Toast.LENGTH_SHORT).show();
                    errorMsg();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void successMsg(){
       final ScrollView sv= (ScrollView)activity.findViewById(R.id.mainScrollableview);
        sv.setVisibility(View.GONE);
        final LinearLayout successLayout=(LinearLayout) activity.findViewById(R.id.success_msg);
        successLayout.setVisibility(View.VISIBLE);
        TextView makeAnotherBooking=(TextView) activity.findViewById(R.id.makeAnotherBookingText);
        LinearLayout errorLayout=(LinearLayout) activity.findViewById(R.id.error_msg);
        errorLayout.setVisibility(View.GONE);
        makeAnotherBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent intent = activity.getIntent();
                    activity.startActivity(intent);
            }
        });
    }

    public static void errorMsg(){
        ScrollView sv= (ScrollView)activity.findViewById(R.id.mainScrollableview);
        sv.setVisibility(View.GONE);
        LinearLayout errorLayout=(LinearLayout) activity.findViewById(R.id.error_msg);
        errorLayout.setVisibility(View.VISIBLE);

        LinearLayout successLayout=(LinearLayout) activity.findViewById(R.id.success_msg);
        successLayout.setVisibility(View.GONE);
    }

}