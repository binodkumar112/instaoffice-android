package com.instaoffice.app.utils;

/**
 * Created by SANCHIT on 8/30/2016.
 */

import android.util.Log;

import com.instaoffice.app.interfaces.Api;
import com.squareup.okhttp.OkHttpClient;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class RestClient {
    private static Api REST_CLIENT;
    private static String ROOT = "http://54.173.206.242:8080/InstaofficeOperations/api/";

    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static Api get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new RestAdapter.Log() {

                    @Override
                    public void log(String msg) {
                        Log.i("Rest log", msg);
                    }
                });


        RestAdapter restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(Api.class);
    }
}
