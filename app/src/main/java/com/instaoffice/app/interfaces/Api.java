package com.instaoffice.app.interfaces;


import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by SANCHIT on 2/6/2017.
 */

public interface Api {
      // get property name
   // http://54.173.206.242:8080/InstaofficeOperations/api/city/ggn/property/list
   //get City Name
    //http://54.173.206.242:8080/InstaofficeOperations/api/city/list
   // http://54.173.206.242:8080/InstaofficeOperations/api/property/1129c030-b695-11e6-913a-cf0c41e88e39/resource/list

   @GET("/city/list")
   void getListOfCities(Callback<ArrayList> callback);

   @GET("/city/{code}/property/list")
   void getSubListOfCitiesSelected(@Path("code") String code, Callback<ArrayList> callback);

    @GET("/property/{propertyId}/resource/list")
    void getMeetingRoomType(@Path("propertyId") String propertyId, Callback<ArrayList> callback);

  @GET("/property/{propertyId}/resource/{resourcecode}/booking")
    void getShedule(@Path("propertyId") String propertyId,@Path("resourcecode") String resourcecode, @Query("from") String from,@Query("to") String to, Callback<ArrayList> callback);

    @FormUrlEncoded
    @POST("/booking/price")
   void getPriceForTheSpecificTime(@Field("phoneno") String phone,@Field("fromdate") String fromtime,@Field("todate") String todate,@Field("propertyid") String propertyid,@Field("resourcecode") String resourcecode,@Field("paymenttype") String paymenttype ,@Field("payload") JSONObject payload, Callback<HashMap> callback);
   // void getPriceForTheSpecificTime(@Header("Authorization")String auth, @Header("Content-Type") String content, @Body String obj, Callback<JSONObject> callback);


    //get All Coupons for user
    @GET("/user/{mobile}/coupons")
    void getAllCouponsForTheUser(@Header("Authorization") String auth,@Path("mobile") String mobileno, Callback<ArrayList> callback);

    //to initiate a Booking
    @FormUrlEncoded
    @POST("/booking")
    void initiateaBookingForTheRoom(@Header("Authorization") String auth, @Field("phoneno") String phone,@Field("fromdate") String fromtime,@Field("todate") String todate,@Field("propertyid") String propertyid,@Field("resourcecode") String resourcecode,@Field("paymenttype") String paymenttype ,@Field("payload") JSONObject payload, Callback<HashMap> callback);

    @FormUrlEncoded
    @POST("/login/generateOtp")
    void sendOTP(@Field("phone") long phone,  @Field("seqid") int seqid, Callback<HashMap> callback);

    @FormUrlEncoded
    @POST("/login/verifyOtp")
    void verifyOTP(@Field("phone") long phone,  @Field("seqid") int seqid, @Field("otp") int otp , Callback<HashMap> callback);

    //deduct credit from wallet when paid through wallet

    //http://54.173.206.242:8080/InstaofficeOperations/api/booking/6102f637-e9f9-410c-aba5-7ed65750aed0/payment
    @FormUrlEncoded
    @POST("/booking/{id}/payment")
    void deductBalanceForPaymentMadeByWallet(@Header("Authorization") String auth, @Field("phoneno") String phoneno,@Field("paymentdata") String paymentdata,@Path("id") String id, Callback<HashMap> callback);


}
