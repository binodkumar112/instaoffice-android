package com.instaoffice.app.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by SANCHIT on 2/8/2017.
 */

public class DetailsOfUser {
    // for the city eg: gurgaon
    public String city;
    public void setCity(String city) {
        this.city = city;
    }
    public String getCity() {
        return city;

    }

    public static String authToken;
    public static void setAuthToken(String authToken) {
        DetailsOfUser.authToken = authToken;
    }
    public static String getAuthToken() {
        return authToken;
    }

    //for the cityCode eg: ggn for gurgaon
    public String cityCode;
    public String getCityCode() {
        return cityCode;
    }
    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    //for property Id eg: for irish tower 6262e150-6cfa-11e6-b34f-6760539490f9
    public static String propertyId;
    public static String getPropertyId() {
        return propertyId;
    }
    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    //for meeting room type eg: for redroom
    public static String meetingRoomResourceCode;
    public static String getMeetingRoomType() {
        return meetingRoomResourceCode;
    }
    public  void setMeetingRoomResourceCode(String meetingRoomResourceCode) {
        this.meetingRoomResourceCode = meetingRoomResourceCode;
    }

    public static JSONObject aminetiesSelectedByUser;
    public static JSONObject getAminetiesSelectedByUser() {
        return aminetiesSelectedByUser;
    }
    public static void setAminetiesSelectedByUser(JSONObject aminetiesSelectedByUser) {
        DetailsOfUser.aminetiesSelectedByUser = aminetiesSelectedByUser;
    }

    public static ArrayList<String> amenotiestype;
    public static ArrayList getAmenotiestypeSwlected() {
        return amenotiestype;
    }
    public static void setAmenotiestype(ArrayList amenotiestype) {
        DetailsOfUser.amenotiestype = amenotiestype;
    }

    public static int totalcostOfUsersRoom;
    public static int getTotalcostOfUsersRoom() {return totalcostOfUsersRoom;}
    public static void setTotalcostOfUsersRoom(int totalcostOfUsersRoom) { DetailsOfUser.totalcostOfUsersRoom = totalcostOfUsersRoom;}

    public static int walletBalanceOfUser;
    public static int getWalletBalanceOfUser() {
        return walletBalanceOfUser;
    }
    public static void setWalletBalanceOfUser(int walletBalanceOfUser) {
        DetailsOfUser.walletBalanceOfUser = walletBalanceOfUser;
    }

    public static  JSONArray am;
    //Start time by user
    public static long startTimeOfBooking;

//End time by user
    public static long endTimeOfBooking;

    //start time of 9.00Am of particular date
    public static long milliSecondsOfStart=0;

    //start time of 6.00Pm of particular date
    public static long milliSecondsOfEnd;

    //All Coupons For Users
    public static ArrayList CouponsForUser;

    public static String userName;
    //public static String mobileNoOfUser;
    public  static String email;

    public static Long MobileNumber;

}
