package com.instaoffice.app.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by SANCHIT on 2/6/2017.
 */

public class CommonModel {

    //whole no. of city data
    public static ArrayList listOfCities;
    public static ArrayList getListOfCities() {
        return listOfCities;
    }
    public  void setListOfCities(ArrayList listOfCities) {
        this.listOfCities = listOfCities;
    }

    //center Detials
public static ArrayList listOfCenters;
    public static ArrayList getListOfCenters() {
        return listOfCenters;
    }
    public  void setListOfCenters(ArrayList listOfCenters) {
        this.listOfCenters = listOfCenters;
    }

    //data to show checkboxs
    public static ArrayList checkBoxDatatodisplay;
    public static ArrayList getCheckBoxDatatodisplay() {
        return checkBoxDatatodisplay;
    }
    public static void setCheckBoxDatatodisplay(ArrayList checkBoxDatatodisplay) {
        CommonModel.checkBoxDatatodisplay = checkBoxDatatodisplay;
    }

    //time on which particular slots are booked

}
